import requests
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry
from bs4 import BeautifulSoup
import json

outputFile = open("result.json", 'w', encoding='utf-8')

retry_strategy = Retry(
    total=5, # Total number of retries
    backoff_factor=1 # Backoff factor for sleep between retries
)

# Create an adapter with the retry strategy
adapter = HTTPAdapter(max_retries=retry_strategy)

# Create a session and mount the adapter to it
http = requests.Session()
http.mount("https://", adapter)
http.mount("http://", adapter)

def flatten_comprehension(matrix):
    return [item for row in matrix for item in row]

n = 0
list_of_ads = []
while n < 27:
    n=n+1
    url = f"https://999.md/ro/list/transport/passenger?page={n}"
    print(url)
    response = http.get(url)
    soup = BeautifulSoup(response.content, 'html.parser')
    soup = soup.find('ul', class_='ads-list-photo large-photo')
    list_of_ads.append(soup.findAll('li'))

flattened_ads = flatten_comprehension(list_of_ads)
done = []
for ad in flattened_ads:
    print(url)
    print(ad)
    anchor = ad.find('div').find('a')
    if not hasattr(anchor, 'find'):
        continue
    imgLink = anchor.find('img')['src']
    url = f"https://999.md{anchor['href']}"
    response = http.get(url)
    soup = BeautifulSoup(response.content, 'html.parser')
    contentSection = soup.find('section', class_="adPage cf")
    title = contentSection.find('h1').text
    username = contentSection.find('a', class_='adPage__aside__stats__owner__login').text
    price = contentSection.find(class_='tooltip adPage__content__price-feature__prices__price').text if contentSection.find(class_='tooltip adPage__content__price-feature__prices__price') else ""
    description = contentSection.find('div', 'adPage__content__description grid_18').text if contentSection.find('div', 'adPage__content__description grid_18') else ""
    phoneNumberAnchors = contentSection.find(class_='js-phone-number adPage__content__phone is-hidden grid_18').findAll('a')
    phoneNumber = []
    for phone in phoneNumberAnchors:
        phoneNumber.append(phone['href'])
    primaryCharacteristics = {}
    directions = []
    prelim1 = contentSection.find(class_='adPage__content__features__col grid_9 suffix_1')
    if prelim1:
        if prelim1.find('h2'):
            if prelim1.find('h2').text == 'Caracteristici principale':
                pcharraw = prelim1.findAll('li')
                for pchar in pcharraw:
                    primaryCharacteristics[pchar.find('span').text.strip()] = pchar.find('span').findNext().text.strip()
        if prelim1.find('h2'):
            if prelim1.find('h2').text == "Direcție":
                directionsraw = prelim1.findAll('li')
                for direction in directionsraw:
                    directions.append(direction.text.strip())

    prelim2 = contentSection.find(class_='adPage__content__features__col grid_7 suffix_1')
    if prelim2:
        if prelim2.find('h2'):
            if prelim2.find('h2').text == 'Caracteristici principale':
                pcharraw = prelim2.findAll('li')
                for pchar in pcharraw:
                    primaryCharacteristics[pchar.find('span').text.strip()] = pchar.find('span').findNext().text.strip()
        if prelim2.find('h2'):
            if prelim2.find('h2').text == "Direcție":
                directionsraw = prelim2.findAll('li')
                for direction in directionsraw:
                    directions.append(direction.text.strip())
    regions = contentSection.find(class_='adPage__content__region grid_18').text
    ad_object = {
        'title':title.strip(),
        'username':username.strip(),
        'price':price.strip(),
        'imgLink':imgLink,
        'desc':description.strip(),
        'directions': directions,
        'primaryCharacteristics':primaryCharacteristics,
        'phoneNumber':phoneNumber,
        'region':regions
    }
    done.append(ad_object)
json.dump(done, outputFile, ensure_ascii=False)


























